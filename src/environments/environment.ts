// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    keyName: 'IPEVO-iBeez-API-KEY',
    keyValue: '5FB7D544D215D99C4C59BB9DAB5CC',
    apiUrl: 'http://api.ibeez.com.tw/'
};
