import { Component, OnInit } from '@angular/core';
import { ChildComponent } from '../child/child.component';
import { ChildTwoComponent } from '../child-two/child-two.component';
@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

