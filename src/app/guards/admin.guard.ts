import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Permission } from '../permission.enum';

@Injectable()
export class AdminGuard implements CanActivate {
    constructor(private router: Router) { }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
            let permission = localStorage.getItem('permission');
            if (permission) {
                if(parseInt(permission) == Permission.Admin){
                    return true;
                }
            }
            this.router.navigate(['/refuse']);
            return false;
    }
}
