import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuardUserComponent } from './guard-user.component';

describe('GuardUserComponent', () => {
  let component: GuardUserComponent;
  let fixture: ComponentFixture<GuardUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuardUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuardUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
