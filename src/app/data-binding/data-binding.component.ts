import { Component, OnInit } from '@angular/core';
import { Permission } from '../permission.enum';

@Component({
    selector: 'app-data-binding',
    templateUrl: './data-binding.component.html',
    styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {
    isValid:boolean = false;
    isBold:boolean = false;
    isSpecial:boolean = false;
    account: string = "Gordon";
    constructor() { }

    ngOnInit() {

    }

    enable(){
        this.isValid = !this.isValid;
    }
    enableClass(){
        this.isBold = !this.isBold;
    }
    enableSpecial(){
        this.isSpecial = !this.isSpecial;
    }

    reset(){
        this.account = "Gordon";
    }

}
