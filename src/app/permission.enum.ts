export enum Permission {
    Guest = 1,
    Normal = 2,
    Admin = 3
}