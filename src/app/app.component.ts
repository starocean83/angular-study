import { Component,OnInit } from '@angular/core';
import { Permission } from './permission.enum';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
    title = 'Angular 讀書會';
    ngOnInit(){
        localStorage.setItem('permission', Permission.Guest.toString());
    }
}
