import { Component, OnInit } from '@angular/core';
import { Permission } from '../permission.enum';
@Component({
    selector: 'app-refuse',
    templateUrl: './refuse.component.html',
    styleUrls: ['./refuse.component.css']
})
export class RefuseComponent implements OnInit {

    constructor() { }
    role: string;
    permission: number;
    ngOnInit() {
        this.permission = parseInt(localStorage.getItem('permission'));
        this.checkRole();
    }

    setNormal(){
        localStorage.setItem('permission', Permission.Normal.toString());
        this.permission = 2;
        this.checkRole();
    }

    setAdmin(){
        localStorage.setItem('permission', Permission.Admin.toString());
        this.permission = 3;
        this.checkRole();
    }

    private checkRole(){
        switch (this.permission){
            case 1:
                this.role = 'Guest';
                break;
            case 2:
                this.role = 'Normal User';
                break;
            case 3:
                this.role = 'Admin';
                break;
            default:
                this.role = 'Guest';
        }
    }
}
