import { Routes, RouterModule } from '@angular/router';
/** component */
import { DataBindingComponent } from './data-binding/data-binding.component';
import { RefuseComponent } from './refuse/refuse.component';
import { GuardUserComponent } from './guard-user/guard-user.component';
import { GuardAdminComponent } from './guard-admin/guard-admin.component';
import { ParentComponent } from './parent/parent.component';
import { IbeezComponent } from './ibeez/ibeez.component';

/** guard */
import { NormalGuard } from './guards/normal.guard';
import { AdminGuard } from './guards/admin.guard';



const appRoutes: Routes = [
	{ path: '', redirectTo: "/home", pathMatch: 'full' },
    { path: 'home', component: DataBindingComponent},
    { path: 'refuse', component: RefuseComponent},
    { path: 'normal', canActivate: [NormalGuard],component: GuardUserComponent},
    { path: 'admin', canActivate: [AdminGuard],component: GuardAdminComponent},
    { path: 'content', component: ParentComponent},
    { path: 'ibeez', component: IbeezComponent},
	// otherwise redirect to home
	{ path: '**', redirectTo: '/home' }
];
export const routing = RouterModule.forRoot(appRoutes);
