import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class LoginService {

    constructor(private http: Http) { }
    login(account: string, password: string): Observable<any> {
        let headers = new Headers();
        let url = environment.apiUrl + "login";
        let urlSearchParams = new URLSearchParams();
        urlSearchParams.append('user_acct', account);
        urlSearchParams.append('user_pass', password);
        urlSearchParams.append('act', "login");
        let body = urlSearchParams.toString()
        headers.append(environment.keyName, environment.keyValue);
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return this.http.post(url, body, { headers: headers })
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}
