import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { BaseService } from './base.service';

@Injectable()
export class UserService extends BaseService {

    constructor(private http: Http) {
        super();
    }

    getUsers(start: number = 0, userID?:string, prefix?:string, detail?:string): Observable<any> {
        let hasToken = this.checkToken();
        if (hasToken) {
            let headers = this.getHeader();
            let url = environment.apiUrl + "users";
            let params: URLSearchParams = new URLSearchParams();
            params.set('token', localStorage.getItem("token"));
            params.set('network_id', localStorage.getItem("networkID"));
            if(prefix) params.set('prefix', prefix);
            if(start) params.set('page_start', start.toString());
            if(userID) params.set('query_user_id', userID);
            if(detail) params.set('detail', detail);
            let search = params.toString();
            return this.http.get(url, { search: decodeURIComponent(search), headers: headers })
                .map(this.extractData)
                .catch(this.handleError);
        } else {
            return Observable.throw("error");
        }
    }
}
