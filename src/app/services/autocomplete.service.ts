import { forEach } from '@angular/router/src/utils/collection';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { TagService } from './tag.service';
import { UserService } from './user.service';

@Injectable()
export class AutocompleteService {
    private _tagRegex = /[#]\S{1,}/g;
    private _atRegex = /[@]\S{1,}/g;
    constructor(private tags: TagService, private users: UserService) { }

    autocomplete(term: string, curPosition:number): Observable<any> {
        if (term !== '') {
            //判斷是否有#
            let matches;
            let match = '';
            while ((matches = this._tagRegex.exec(term)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (matches.index === this._tagRegex.lastIndex) {
                    this._tagRegex.lastIndex++;
                }
                if(curPosition >= matches.index && curPosition <= this._tagRegex.lastIndex){
                    match = matches[0];
                    this._tagRegex.lastIndex = 0;
                    break;
                }
            }
            if (match) {
                let prefix = match.replace("#","");
                return this.tags.getTags(prefix).map(data => {
                    data.type = "tag";
                    data.match = match;
                    return data;
                });
            }

            //判斷是否有@
            while ((matches = this._atRegex.exec(term)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (matches.index === this._atRegex.lastIndex) {
                    this._atRegex.lastIndex++;
                }
                if(curPosition >= matches.index && curPosition <= this._atRegex.lastIndex){
                    match = matches[0];
                    this._atRegex.lastIndex = 0;
                    break;
                }
            }
            if (match) {
                let prefix = match.replace("@","");
                return this.users.getUsers(0,'',prefix).map(data => {
                    data.type = "user";
                    data.match = match;
                    return data;
                });
            }

            if(!match) {
                return Observable.of({});
            }
        } else {
            return Observable.of({});
        }
    }
}
