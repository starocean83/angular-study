import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { BaseService } from './base.service';
import 'rxjs/Rx';

@Injectable()
export class TagService extends BaseService {

    constructor(private http: Http) {
        super();
    }

    getTags(prefix?:string, start?: number): Observable<any> {
        let hasToken = this.checkToken();
        if (hasToken) {
            let headers = this.getHeader();
            let url = environment.apiUrl + "tags";
            let params: URLSearchParams = new URLSearchParams();
            params.set('token', localStorage.getItem("token"));
            params.set('network_id', localStorage.getItem("networkID"));
            if(prefix) params.set('prefix', prefix);
            if(start) params.set('page_start', start.toString());
            let search = params.toString();
            return this.http.get(url, { search: decodeURIComponent(search), headers: headers })
                .map(this.extractData)
                .catch(this.handleError);
        } else {
            return Observable.throw("error");
        }
    }

}
