import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { BaseService } from './base.service';
import 'rxjs/Rx';

@Injectable()
export class GroupService extends BaseService{

    constructor(private http: Http) {
        super();
    }

    getMyGroup(): Observable<any>{
        return this.getGroups("true");
    }

    getGroups(myGroup:string="false", groupID?: string, sort?: string, order?:string): Observable<any> {
        let hasToken = this.checkToken();
        if(hasToken){
            let headers = this.getHeader();
            let url = environment.apiUrl + "groups";
            let params: URLSearchParams = new URLSearchParams();
            params.set('token', localStorage.getItem("token"));
            params.set('network_id', localStorage.getItem("networkID"));
            params.set('is_my_group', myGroup);
            return this.http.get(url, {search:params.toString(), headers: headers})
                .map(this.extractData)
                .catch(this.handleError);
        } else {
            return Observable.throw("error");
        }
    }
}
