import { Injectable } from '@angular/core';
import { Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

import 'rxjs/Rx';
@Injectable()
export class BaseService {

    constructor() { }

    public checkToken():boolean{
        if(localStorage.getItem("token") && localStorage.getItem("networkID")){
            return true;
        }
        return false;
    }

    public getHeader(): Headers{
        let headers = new Headers();
        headers.append(environment.keyName, environment.keyValue);
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        return headers;
    }

    protected extractData(res: Response) {
        let body = res.json();
        if(body.errorCode === '401'){
            localStorage.removeItem("networkID");
            localStorage.removeItem("token");
            throw new Error('Bad response status: ' + body.message);
        } else {
            return body || {};
        }
    }

    protected handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }
}
