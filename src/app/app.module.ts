import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';

import { NormalGuard } from './guards/normal.guard';
import { AdminGuard } from './guards/admin.guard';
import { LoginService } from './services/login.service';
import { GroupService } from './services/group.service';
import { TagService } from './services/tag.service';
import { UserService } from './services/user.service';
import { AutocompleteService } from './services/autocomplete.service';

import { AppComponent } from './app.component';
import { DataBindingComponent } from './data-binding/data-binding.component';
import { GuardUserComponent } from './guard-user/guard-user.component';
import { GuardAdminComponent } from './guard-admin/guard-admin.component';
import { RefuseComponent } from './refuse/refuse.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { ChildTwoComponent } from './child-two/child-two.component';
import { IbeezComponent } from './ibeez/ibeez.component';

@NgModule({
    declarations: [
        AppComponent,
        DataBindingComponent,
        GuardUserComponent,
        GuardAdminComponent,
        RefuseComponent,
        ParentComponent,
        ChildComponent,
        ChildTwoComponent,
        IbeezComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        routing
    ],
    providers: [
        NormalGuard,
        AdminGuard,
        LoginService,
        GroupService,
        TagService,
        UserService,
        AutocompleteService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
