import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LoginService } from '../services/login.service';
import { GroupService } from '../services/group.service';
import { AutocompleteService } from '../services/autocomplete.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-ibeez',
    templateUrl: './ibeez.component.html',
    styleUrls: ['./ibeez.component.css']
})
export class IbeezComponent implements OnInit {
    @ViewChild("myTextArea") msgBox:ElementRef;
    loginForm: FormGroup;
    message: string = '';
    isLogin: boolean;
    groups: Array<any>;
    term = new FormControl();
    tagList = Array<object>();
    tagMatch:string = '';
    userList = Array<object>();
    userMatch:string = '';
    caretPos:number = 0;
    constructor(private lgService: LoginService, private gpService: GroupService,
        private fb: FormBuilder, private as: AutocompleteService)
    {
        this.loginForm = fb.group({
            // 定義表格的預設值
            'account': ["gordontseng@ipevo.com", [Validators.required, Validators.email]],
            'password': ["", Validators.required],
        })

        if (localStorage.getItem('token')) {
            this.isLogin = true;
        } else {
            this.isLogin = false;
        }
        this.term
            .valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .switchMap(term => {
                this.tagList = [];
                this.tagMatch = '';
                this.userList = [];
                this.userMatch = '';
                return this.as.autocomplete(term,this.caretPos);
            })
            .subscribe(
                result => this.termListen(result),
                error => { }
            );
    }

    ngOnInit() {
        if (this.isLogin) {
            this.getMyGroup();
        }
    }

    getMyGroup() {
        this.gpService.getMyGroup().subscribe(
            data => {
                this.groups = data.response.groups;
            },
            err => {
                alert(err);
            },
        );
    }

    loginApi() {
        let fv = this.loginForm.value;
        this.lgService
            .login(fv.account, fv.password)
            .subscribe(data => {
                if (data.status) {
                    this.isLogin = true;
                    let token = data.user_info.token;
                    let networkID = data.user_info.network_id;
                    localStorage.setItem('token', token);
                    localStorage.setItem('networkID', networkID);
                    this.message = "";
                    this.gpService.getMyGroup().subscribe(data => {
                        this.groups = data.response.groups;
                    });
                } else {
                    this.message = data.message;
                }
            });
    }

    getCaretPos(oField) {
        if (oField.selectionStart || oField.selectionStart == '0') {
            this.caretPos = oField.selectionStart;
        }
    }

    termListen(result: any) {
        if (result.status == true) {
            if (result.type == 'tag') {
                this.tagList = result.response.tags;
                this.tagMatch = result.match;
            }
            if (result.type == 'user') {
                this.userList = result.response.users;
                this.userMatch = result.match;
            }
        }
    }

    tagClick(tagName:string){
        if(this.tagMatch){
            let msg = this.term.value;
            let startIndex = this.caretPos - this.tagMatch.length;
            let lastIndex = startIndex + this.tagMatch.length;
            let replace = "#" + tagName + " ";
            msg = msg.substring(0, startIndex) + replace + msg.substring(lastIndex, msg.length);
            this.term.setValue(msg);
            this.msgBox.nativeElement.focus();
            this.caretPos = msg.length;
        }
        this.tagMatch = '';
        this.tagList = [];
    }

    userClick(account:string){
        if(this.userMatch){
            let msg = this.term.value;
            let startIndex = this.caretPos - this.userMatch.length;
            let lastIndex = startIndex + this.userMatch.length;
            let replace = "@" + account + " ";
            msg = msg.substring(0, startIndex) + replace + msg.substring(lastIndex, msg.length);
            this.term.setValue(msg);
            this.msgBox.nativeElement.focus();
            this.caretPos = msg.length;
        }
        this.userMatch = '';
        this.userList = [];
    }
}
