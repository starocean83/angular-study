import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IbeezComponent } from './ibeez.component';

describe('IbeezComponent', () => {
  let component: IbeezComponent;
  let fixture: ComponentFixture<IbeezComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IbeezComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IbeezComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
